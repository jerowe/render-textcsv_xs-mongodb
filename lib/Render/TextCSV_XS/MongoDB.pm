package Render::TextCSV_XS::MongoDB;

use 5.010000;
use Carp;
use Class::MOP;
use Data::Dumper;
use Tie::IxHash;
use MongoDB;
use Moose::Role;

require Exporter;

our $VERSION = '0.01';

sub setup_mongodb{
    my $self = shift;

    if($self->mongodb_db && $self->mongodb_collection){
        return;
    }
    elsif($self->mongodb_db && !$self->mongodb_collection){
        my $col = $self->mongodb_db->get_collection( 'bar' );
        $self->mongodb_collection($col);
        return;
    }
    else{
        my $db = $self->mongodb->get_database( 'foo' );
        my $col = $db->get_collection( 'bar' );
        $self->mongodb_db($db);
        $self->mongodb_collection($col);
        return;
    }
}

sub populate{
    my $self = shift;
    my $collection = $self->mongodb_collection;

    my $cnames = $collection->get_collection('column_names');

    $collection->get_collection('column_names')->insert({name => [$self->textcsv_xs->column_names]});

    while (my $row = $self->textcsv_xs->getline_hr ($self->_fh)) {
        next unless $row;
        my $line = join($self->sep_char, values %{$row});
        if($line =~ qr/$self->skip_pattern/){
            next;
        }
        else{
#            print "Row is ".Dumper($row);
            $self->mongodb_collection->insert($row);
        }
    }
}

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

Render::TextCSV_XS::MongoDB - Take a deliminated file and use it to populate a MongoDB instance

=head1 SYNOPSIS

    package MyTable;

    use Moose;
    extends 'Render';
    with 'Render::MongoDB';
    with 'Render::TextCSV_XS::MongoDB';
    with 'Render::Dgrid::MongoDB';

#    TextCSV part
    my $dt = MyTable->new();
    my $file = "/path/to/file.gz"; #Handles both gziped and regular files
    my $skip_pattern;
    $skip_pattern ='^\\s*##';
#    $skip_pattern = "^\s*##"; #Can be written this way as well
#    $skip_pattern = qr/^\s*##/; #Or this way

    $dt->infile($file);
    $dt->sep_char("\t");
    $dt->textcsv_args( { allow_loose_quotes => 1, binary => 1, auto_diag => 1, sep_char => "\t"} );
    $dt->skip_pattern($skip_pattern);
    $dt->setup();

#    MongoDB Part
    $dt->setup_mongodb();
    $dt->populate();
    my @columns = $dt->textcsv_xs->column_names;
    print Dumper(\@columns);
    my $collection = $dt->mongodb_collection;
    print Dumper($collection);

    my $limit = 5;
    my $page = 1;

    my $all_records = $dt->mongodb_collection->find->limit($limit)->skip(($page-1) * $limit);

    $dt->stream($all_records);
    $dt->process();

    print Dumper($dt->dgcolumn);
    print Dumper($dt->dgdata);

    $dt->mongodb_db->drop;


=head1 DESCRIPTION

Use this module to populate a MongoDB collection with a textcsv table. The table can then be parsed into an object for Dgrid.

=head2 EXPORT

None by default.


=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

Jillian Rowe, E<lt>jillian.e.rowe@gmail.com<gt>


=head1 COPYRIGHT AND LICENSE

Copyright (C) 2013 by Jillian Rowe

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.16.3 or,
at your option, any later version of Perl 5 you may have available.


=cut
